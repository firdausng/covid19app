sudo docker build -t covid/app/web  -f src/Web/Dockerfile  .


```
sudo docker system prune -f
sudo docker network create --subnet 10.10.0.0/24 service-network
sudo docker run --name postgres --network service-network -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres

sudo docker run --detach \
  --restart always \
  --publish 80:80 \
  --publish 443:443 \
  --name nginx-proxy \
  --network service-network \
  --volume /var/run/docker.sock:/tmp/docker.sock:ro \
  --volume nginx-certs:/etc/nginx/certs \
  --volume nginx-vhost:/etc/nginx/vhost.d \
  --volume nginx-html:/usr/share/nginx/html \
  jwilder/nginx-proxy

sudo docker run --detach \
  --restart always \
  --name nginx-proxy-letsencrypt \
  --network service-network \
  --volume /var/run/docker.sock:/var/run/docker.sock:ro \
  --volume nginx-certs:/etc/nginx/certs \
  --volume nginx-vhost:/etc/nginx/vhost.d \
  --volume nginx-html:/usr/share/nginx/html \
  --env NGINX_PROXY_CONTAINER="nginx-proxy" \
  jrcs/letsencrypt-nginx-proxy-companion
  
sudo docker run --name web \
  --network service-network \
  -e "ConnectionStrings__DefaultConnection=Host=postgres;Database=CovidDb;Username=postgres;Password=postgres" \
  -e "VIRTUAL_HOST=demo1.firdausng.com" \
  -e "LETSENCRYPT_HOST=demo1.firdausng.com" \
  -e "LETSENCRYPT_EMAIL=firdausng@firdausng.com" \
  -d covid/app/web
  ```