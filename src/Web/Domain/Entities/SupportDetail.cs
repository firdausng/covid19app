﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Domain.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }

    public class SupportDetail: BaseEntity
    {
        public Guid UserId { get; set; }
        public string UserEmail { get; set; }
        public SupportStatus Status { get; set; }
        public string StatusReason { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public RequestType RequestType { get; set; }

    }

    public enum SupportStatus
    {
        Approved,
        Pending,
        Rejected
    }

    public enum RequestType
    {
        Medicine = 1,
        Food = 2,
        Cash = 3,
        Other = 100
    }
}
