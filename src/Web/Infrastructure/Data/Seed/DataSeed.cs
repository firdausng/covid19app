﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Infrastructure.Data.Seed
{
    public class DataSeed
    {
        private readonly ApplicationDbContext context;
        private readonly ILogger<DataSeed> logger;

        public DataSeed(ApplicationDbContext applicationDbContext, ILogger<DataSeed> logger)
        {
            this.context = applicationDbContext;
            this.logger = logger;
        }

        public void EnsureSeedData()
        {
            logger.LogInformation("Migrating db");
            context.Database.Migrate();
        }
    }
}
