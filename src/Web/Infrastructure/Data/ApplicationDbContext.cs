﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Domain.Entities;

namespace Web.Infrastructure.Data
{
    public class ApplicationDbContext: DbContext
    {
        public DbSet<SupportDetail> SupportDetails { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }


    }
}
