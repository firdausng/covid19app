﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Infrastructure.Data.Migrations
{
    public partial class addstatusreason : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StatusReason",
                table: "SupportDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StatusReason",
                table: "SupportDetails");
        }
    }
}
