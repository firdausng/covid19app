﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Infrastructure.Data.Migrations
{
    public partial class addemailtosupportdetail : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "UserEmail",
                table: "SupportDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserEmail",
                table: "SupportDetails");
        }
    }
}
