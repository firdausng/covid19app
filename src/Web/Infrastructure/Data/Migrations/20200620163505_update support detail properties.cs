﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Web.Infrastructure.Data.Migrations
{
    public partial class updatesupportdetailproperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AdditionalInfo",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "City",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Gender",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Income",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "LastName",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Passport",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Phone",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "PhysicallyChalleged",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "State",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Zip",
                table: "SupportDetails");

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "SupportDetails",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "SupportDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RequestType",
                table: "SupportDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "SupportDetails",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Address",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "RequestType",
                table: "SupportDetails");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "SupportDetails");

            migrationBuilder.AddColumn<string>(
                name: "AdditionalInfo",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "SupportDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gender",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Income",
                table: "SupportDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "LastName",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Passport",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Phone",
                table: "SupportDetails",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "PhysicallyChalleged",
                table: "SupportDetails",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "SupportDetails",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Zip",
                table: "SupportDetails",
                type: "text",
                nullable: true);
        }
    }
}
