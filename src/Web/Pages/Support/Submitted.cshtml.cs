﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Web.Infrastructure.Data;

namespace Web.Pages.Support
{
    public class SubmittedModel : PageModel
    {
        private readonly ApplicationDbContext context;

        public SubmittedModel(ApplicationDbContext context)
        {
            this.context = context;
        }

        public void OnGet()
        {

        }
    }
}