﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Web.Infrastructure.Data;

namespace Web.Pages.Support
{
    public class StatusModel : PageModel
    {
        private readonly ApplicationDbContext context;

        [BindProperty]
        public UserSupportDetailVm UserSupportDetailVm { get; set; }

        public StatusModel(ApplicationDbContext context)
        {
            this.context = context;
        }
        public async Task<IActionResult> OnGet()
        {
            var userId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value);
            var supportRequested = await context.SupportDetails.FirstOrDefaultAsync(s => s.UserId.Equals(userId));

            if (supportRequested == null)
            {
                return Redirect("/Support");
            }

            UserSupportDetailVm = new UserSupportDetailVm
            {
                Name = supportRequested.Title,
                Status = supportRequested.Status.ToString()
            };

            if (!string.IsNullOrEmpty(supportRequested.StatusReason))
            {
                UserSupportDetailVm.Reason = supportRequested.StatusReason;
            }

            return Page();
        }
    }

    public class UserSupportDetailVm
    {
        public string Name { get; set; }
        public string Status { get; set; }
        public string Reason { get; set; }
    }
}