﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Web.Areas.Identity.Data;
using Web.Domain.Entities;
using Web.Infrastructure.Data;

namespace Web.Pages.Support
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [DataType(DataType.Text)]
            [Display(Name = "Title")]
            public string Title { get; set; }
            [Required]
            [DataType(DataType.MultilineText)]
            [Display(Name = "Address")]
            [MaxLength(500)]
            public string Address { get; set; }
            [Required]
            [DataType(DataType.MultilineText)]
            [Display(Name = "Description")]
            [MaxLength(500)]
            public string Description { get; set; }

            [Required]
            [Display(Name = "Request Type")]
            public RequestTypeDto RequestType { get; set; }

            public string Email { get; set; }
        }

        public enum RequestTypeDto
        {
            Medicine = 1,
            Food = 2,
            Cash = 3,
            Other = 100
        }

        public IndexModel(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
        }

        public async Task<IActionResult> OnGet()
        {
            var userId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value);
            var supportRequested = await context.SupportDetails.Where(s => s.UserId.Equals(userId)).ToListAsync();
            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            if (ModelState.IsValid)
            {
                var userId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value);
                var user = await userManager.FindByIdAsync(userId.ToString());
                context.SupportDetails.Add(new SupportDetail 
                { 
                    Title = Input.Title,
                    RequestType = (RequestType)RequestType.Parse(typeof(RequestType), Input.RequestType.ToString()),
                    Address = Input.Address,
                    Description = Input.Description,
                    Status = SupportStatus.Pending,
                    UserEmail = user.Email,
                    UserId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value)
                });
                await context.SaveChangesAsync();
                return Redirect("/Index");
            }
            return Page();
        }
    }

    public enum GenderType
    {
        Male,
        Female
    }
}