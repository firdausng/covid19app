﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Web.Areas.Identity.Data;
using Web.Domain.Entities;
using Web.Infrastructure.Data;

namespace Web.Pages.Admin
{
    public class ApplicationDetailModel : PageModel
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            public Guid RequestId { get; set; }
            public SupportStatus Status { get; set; }
            public string StatusReason { get; set; }
            public string Title { get; set; }
            public string Address { get; set; }
            public string Description { get; set; }
            public RequestType RequestType { get; set; }


            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string IdentityCardNumber { get; set; }
            public int Age { get; set; }
            public Gender Gender { get; set; }
            public double MonthlyIncome { get; set; }
            public string Phone { get; set; }
            public string Email { get; set; }
        }

        public ApplicationDetailModel(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
            this.Input = new InputModel();
        }

        public async Task OnGet(Guid id)
        {
            var supportDetail = await context.SupportDetails.FirstOrDefaultAsync(s => s.Id.Equals(id));
            var user = await userManager.FindByIdAsync(supportDetail.UserId.ToString());
            
            Input.FirstName = user.FirstName;
            Input.LastName = user.LastName;
            Input.Email = user.Email;
            Input.IdentityCardNumber = user.IdentityCardNumber;
            Input.Age = user.Age;
            Input.Gender = user.Gender;
            Input.Phone = user.PhoneNumber;
            Input.MonthlyIncome = user.MonthlyIncome;

            Input.RequestId = id;
            Input.Title = supportDetail.Title;
            Input.RequestType = supportDetail.RequestType;
            Input.Status = supportDetail.Status;
            Input.StatusReason = supportDetail.StatusReason;
            Input.Address = supportDetail.Address;
            Input.Description = supportDetail.Description;
        }

        public async Task<IActionResult> OnPost(Guid id)
        {
            var supportDetailEntity = await context.SupportDetails.FirstOrDefaultAsync(s => s.Id.Equals(id));
            supportDetailEntity.Status = Input.Status;
            supportDetailEntity.StatusReason = Input.StatusReason;
            context.SupportDetails.Update(supportDetailEntity);
            await context.SaveChangesAsync();
            return Redirect("/admin");
        }
    }
}