﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Web.Areas.Identity.Data;
using Web.Domain.Entities;
using Web.Infrastructure.Data;

namespace Web.Pages.Admin
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext context;
        private readonly UserManager<ApplicationUser> userManager;

        public List<SupportDetail> SupportDetails { get; set; }

        public IndexModel(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            this.context = context;
            this.userManager = userManager;
            SupportDetails = context.SupportDetails.OrderByDescending(s => s.Title).ToList();
        }

        public void OnGet()
        {

        }

        public void onPost()
        {

        }

        public async Task OnPostApprove(Guid id)
        {
            var supportDetailEntity = await context.SupportDetails.SingleOrDefaultAsync(s => s.Id.Equals(id));
            supportDetailEntity.Status = SupportStatus.Approved;
            await context.SaveChangesAsync();
        }

        public async Task OnPostReject(Guid id)
        {
            var supportDetailEntity = await context.SupportDetails.SingleOrDefaultAsync(s => s.Id.Equals(id));
            supportDetailEntity.Status = SupportStatus.Rejected;
            await context.SaveChangesAsync();
        }

    }
}