﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Web.Infrastructure.Data;

namespace Web.Pages
{
    [Authorize()]
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly ApplicationDbContext context;

        [BindProperty]
        public List<ViewModel> Vm { get; set; } = new List<ViewModel>();

        public class ViewModel
        {
            public string Status { get; set; }
            public string StatusReason { get; set; }
            public string Title { get; set; }
            public string RequestType { get; set; }
        }

        public IndexModel(ILogger<IndexModel> logger, ApplicationDbContext context)
        {
            _logger = logger;
            this.context = context;
        }

        public async Task<IActionResult> OnGet()
        {
            var userId = Guid.Parse(User.Claims.FirstOrDefault(c => c.Type.Equals("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")).Value);
            var supportDetails = await context.SupportDetails
                .Where(s => s.UserId.Equals(userId))
                .OrderByDescending(s => s.Title)
                .ToListAsync();

            Vm = supportDetails.Select(s => new ViewModel
                {
                    RequestType = s.RequestType.ToString(),
                    Status = s.Status.ToString(),
                    StatusReason = s.StatusReason,
                    Title = s.Title
                })
                .ToList();

            return Page();
        }
    }
}
