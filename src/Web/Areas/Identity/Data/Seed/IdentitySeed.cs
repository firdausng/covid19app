﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Web.Areas.Identity.Data.Authorization;
using Web.Infrastructure.Data;

namespace Web.Areas.Identity.Data.Seed
{
    public class IdentitySeed
    {
        private readonly IdentityContext context;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly ILogger<IdentitySeed> logger;

        public IdentitySeed(IdentityContext context, UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, ILogger<IdentitySeed> logger)
        {
            this.context = context;
            this.userManager = userManager;
            this.roleManager = roleManager;
            this.logger = logger;
        }

        public async Task EnsureSeedData()
        {
            logger.LogInformation("Migrating db");
            context.Database.Migrate();


            var admin = new ApplicationUser
            {
                UserName = "admin",
                Email = "admin@covidapp.com",
                FirstName = "Super",
                LastName = "Admin",
                Age = 0,
                Gender= Gender.Male,
                EmailConfirmed = true,
            };

            var adminID = await EnsureUser(admin, "Password@01");
            await EnsureRole(adminID, Constants.AdministratorsRole);


            var user = new ApplicationUser
            {
                UserName = "user",
                Email = "user@covidapp.com",
                FirstName = "Normal",
                LastName = "user",
                Age = 24,
                Gender = Gender.Male,
                MonthlyIncome = 2000,
                PhoneNumber = "+610012121232",
                IdentityCardNumber = "000000000000",
                EmailConfirmed = true,

            };
            var registeredUserID = await EnsureUser(user, "Password@01");
            await EnsureRole(registeredUserID, Constants.RegisteredUserRole);

        }


        private async Task<string> EnsureUser(ApplicationUser userDetail, string testUserPw)
        {
            var user = await userManager.FindByNameAsync(userDetail.UserName);
            if (user == null)
            {
                user = userDetail;
                var result = await userManager.CreateAsync(user, testUserPw);
                logger.LogInformation($"{userDetail.UserName} user created");
            }

            if (user == null)
            {
                throw new Exception("The password is probably not strong enough!");
            }

            

            return user.Id;
        }

        private async Task<IdentityResult> EnsureRole(string uid, string role)
        {
            IdentityResult IR = null;

            if (!await roleManager.RoleExistsAsync(role))
            {
                IR = await roleManager.CreateAsync(new IdentityRole(role));
                logger.LogInformation($"{role} role created");
            }

            var user = await userManager.FindByIdAsync(uid);

            if (user == null)
            {
                throw new Exception("The user password was probably not strong enough!");
            }

            IR = await userManager.AddToRoleAsync(user, role);
            logger.LogInformation($"{user} is added to {role} role");

            return IR;
        }
    }
}
